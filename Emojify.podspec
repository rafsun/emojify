#
# Be sure to run `pod lib lint Emojify.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Emojify'
  s.version          = '0.1.4'
  s.summary          = 'Emojify is to support embedding custom emoji/unicode charecters to text.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Emojify is to support embedding custom emoji/unicode charecters to text as image attachments. It also gives you supprted emoji bundle list, recently used emoji list and more.
                       DESC

  s.homepage         = 'https://bitbucket.org/gaga-gugu/emojify'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'COPYRIGHTED', :file => 'LICENSE' }
  s.author           = { 'Ashik Ahmad' => 'ashik.ahmad@gagagugu.com' }
  s.source           = { :git => 'git@bitbucket.org:gaga-gugu/emojify.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'Emojify/Classes/**/*'
  
  s.resource_bundles = {
    'Emojify' => ['Emojify/Assets/Images/**/*.png', 'Emojify/Assets/JSON/*.json']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'SwiftyJSON'
end
