//
//  UITextField_Emoji.swift
//  Connect
//
//  Created by Ashik Ahmad on 10/28/16.
//  Copyright © 2016 Gagagugu. All rights reserved.
//

import UIKit

public extension UITextView {
    
    public func emojify(with emojiManager: GGEmoji) {
        var selection = selectedTextRange
        let cursorPosition = offset(from: beginningOfDocument, to: selection?.start ?? endOfDocument)
        var cursorOffset = 0
        
        if let font = font {
            // For expressions
            attributedText = emojiManager.emojifiedString(attributedText!, font: font, handler: { (replacedEmoji, range, destLen) in
                if range.location < cursorPosition {
                    cursorOffset += destLen - range.length
                }
            })
            // For Emoji glyphs
            attributedText = emojiManager.imagifiedString(attributedText!, font: font, handler: { (replacedEmoji, sourceRange, destinationLength) in
                if sourceRange.location < cursorPosition {
                    cursorOffset += destinationLength - sourceRange.length
                }
            })
            self.font = font
        }
        
        if cursorOffset != 0 {
            if let sel = selection,
                let fixedFromPos = position(from: sel.start, offset: cursorOffset),
                let fixedToPos = position(from: sel.end, offset: cursorOffset)
            {
                selection = textRange(from: fixedFromPos, to: fixedToPos)
            }
        }
        
        selectedTextRange = selection
    }
    
    public func emojiStrippedText(with emojiManager: GGEmoji)->String {
        return emojiManager.emojiStripedString(self.attributedText).string
    }
}
