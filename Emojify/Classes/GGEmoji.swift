//
//  GGEmoji.swift
//  Connect
//
//  Created by Ashik Ahmad on 10/27/16.
//  Copyright © 2016 Gagagugu. All rights reserved.
//

/*
 JSON data is collected from:
 https://github.com/iamcal/js-emoji/blob/master/lib/emoji.js
 */
import Foundation
import SwiftyJSON

public class GGEmoji {
    public var recentEmojis: [EmojiInfo] = []
    public var recentEmojisLimit = 50 {
        didSet {
            trimRecentHistory()
        }
    }
    
    public fileprivate(set) var emojiBundles: [EmojiBundle] = []
    
    /// <key>:<unicode>
    public fileprivate(set) var keyMap: [String: String] = [:]
    /// <unicode-char>:<image-name>
    public fileprivate(set) var imageMap: [String:String] = [:]
    /// <expression>:<unicode-char>
    public fileprivate(set) var expressionMap: [String:String] = [:]
    
    /// Regex to match supported expressions to convert them to respective unicode charecters
    public fileprivate(set) var expressionRegex: NSRegularExpression? = nil
    /// Regex to match unicode charecters with available image replacement
    public fileprivate(set) var imageEmojiRegex: NSRegularExpression? = nil
    
    public init () {
        loadRecentHistory()
        
        // Parse full emoji map
        if let jsonMap = loadJSON("EmojiMap")  as? [String: [Any]] {
            for (key, infoArray) in jsonMap {
                if let codeArr = infoArray.first as? [String],
                    let emoChar = codeArr.first {
                    keyMap[key.uppercased()] = emoChar
                    
                    if infoArray.count > 4 {
                        if let expressions = infoArray[3] as? [String] {
                            for expression in expressions {
                                expressionMap[":"+expression+":"] = emoChar
                            }
                        }
                    }
                }
            }
        }
        
        // Parse bundles
        if let json = loadJSON("EmojiImageBundles")  as? [[String: Any]] {
            for imgBundleData in json {
                if let name = imgBundleData["bundle-name"] as? String,
                    let icon = imgBundleData["bundle-icon"] as? String,
                    let emojis = imgBundleData["emojis"] as? [[String:String]] {
                    let imgBundle = EmojiBundle(name: name, iconName: icon)
                    for emojiData in emojis {
                        if let key = emojiData["key"]?.uppercased(),
                            let image = emojiData["image"] {
                            let emoji = keyMap[key]
                            if let emoji = emoji {
                                imageMap[emoji] = image
                                let emojiInfo = EmojiInfo(key: key, imageName: image, emoji: emoji)
                                imgBundle.emojis.append(emojiInfo)
                            } else {
                                // Not supported
                                print(key)
                            }
                        }
                    }
                    emojiBundles.append(imgBundle)
                }
            }
        }
        
        let commonEmojiExp = "(:[a-z0-9-+_\\-]+:)"
        var regexParts = [commonEmojiExp]
        
        if let codeJson = loadJSON("EmojiShortCode") as? [String: String] {
            for (shortCode, exp) in codeJson {
                if let unicodeEmoji = expressionMap[":"+exp+":"] {
                    expressionMap[shortCode] = unicodeEmoji
                    regexParts.append("("+NSRegularExpression.escapedPattern(for: shortCode)+")")
                }
            }
        }
        
        var regexStr = regexParts.joined(separator: "|")
        regexStr = "(?<=^|\\s)(" + regexStr + ")(?=\\s)"
        expressionRegex = try? NSRegularExpression(pattern: regexStr, options: [])
        
        let supportedEmojies = Array(imageMap.keys)
        let emoStr = supportedEmojies.joined(separator: "|")
        imageEmojiRegex = try? NSRegularExpression(pattern: emoStr, options: [])
    }
    
    //---------------------------------------------------
    // MARK: - Public methods
    //---------------------------------------------------
    
    public func hasImageEmojis(_ string:String)->Bool {
        if let regex = imageEmojiRegex {
            let emoMatches = regex.matches(in: string as String, options: [], range: NSMakeRange(0, string.characters.count))
            if emoMatches.count > 0 {
                return true
            }
        }
        
        return false
    }
    
    public func emojifiedString(_ string:NSAttributedString, font:UIFont? = nil, handler:((_ replacedEmoji:String, _ sourceRange:NSRange, _ destinationLength: Int)->Void)? = nil)->NSAttributedString {
        
        let rawStr = string.string as NSString
        let mutableString = string.mutableCopy() as! NSMutableAttributedString
        
        if let regex = expressionRegex {
            let emoMatches = regex.matches(in: rawStr as String, options: [], range: NSMakeRange(0, rawStr.length))
            var offset = 0
            
            for match in emoMatches {
                var fixedRange = match.range
                fixedRange.location += offset
                
                let exp = mutableString.attributedSubstring(from: fixedRange).string
                
                var replacementString:NSAttributedString? = nil
                if let emoChar = expressionMap[exp] {
                    var imagified = false
                    // Has Image: change with image
                    if let imageName = imageMap[emoChar],
                        let image = UIImage(emoji: imageName) {
                        let attachment = EmojiAttachment(image, key: emoChar, font: font)
                        replacementString = NSAttributedString(attachment: attachment)
                        imagified = true
                    }
                    // Don't have image: change with emoji char
                    else {
                        replacementString = NSAttributedString(string: emoChar)
                    }
                    
                    if let replacer = replacementString {
                        mutableString.replaceCharacters(in: fixedRange, with: replacer)
                        
                        let destLen = imagified ? 1 : (emoChar as NSString).length
                        let srcLen = (exp as NSString).length
                        
                        handler?(emoChar, fixedRange, destLen)
                        
                        offset +=  destLen - srcLen
                    }
                }
            }
        }
        
        return mutableString
    }
    
    public func imagifiedString(_ string:NSAttributedString, font:UIFont? = nil, handler:((_ replacedEmoji:String, _ sourceRange:NSRange, _ destinationLength: Int)->Void)? = nil)->NSAttributedString {
        let rawStr = string.string as NSString
        let mutableString = string.mutableCopy() as! NSMutableAttributedString
        
        if let regex = imageEmojiRegex {
            let emoMatches = regex.matches(in: rawStr as String, options: [], range: NSMakeRange(0, rawStr.length))
            var offset = 0
            
            for match in emoMatches {
                var fixedRange = match.range
                fixedRange.location += offset
                
                let emoChar = mutableString.attributedSubstring(from: fixedRange).string
                
                if let imageName = imageMap[emoChar],
                    let image = UIImage(emoji: imageName)
                {
                    let attachment = EmojiAttachment(image, key: emoChar, font: font)
                    let replacementStr = NSAttributedString(attachment: attachment)
                    
                    mutableString.replaceCharacters(in: fixedRange, with: replacementStr)
                    
                    handler?(emoChar, fixedRange, 1)
                    let srcLen = (emoChar as NSString).length
                    offset += 1 - srcLen
                }
            }
        }
        
        return mutableString
    }
    
    public func countEmojiCharacter(_ string:NSAttributedString, font:UIFont? = nil, handler:((_ replacedEmoji:String, _ sourceRange:NSRange, _ destinationLength: Int)->Void)? = nil)->Int {
        let rawStr = string.string as NSString
        _ = string.mutableCopy() as! NSMutableAttributedString
        
        var count = 0
        
        if let regex = imageEmojiRegex {
            let emoMatches = regex.matches(in: rawStr as String, options: [], range: NSMakeRange(0, rawStr.length))
            
            
            //for _ in emoMatches {
               count =  emoMatches.count
            //}
        }
        
        return count
    }

    
    public func emojiStripedString(_ string:NSAttributedString)->NSAttributedString {
        let str = string.mutableCopy() as! NSMutableAttributedString
        
        // NOTE: `offset` is to fix range for a reason that most unicode emojis are counted as
        // two char while text attachment is counted as one. So every such replacement needs
        // one char offset from original calculated range.
        var offset = 0
        string.enumerateAttribute(NSAttachmentAttributeName, in: NSMakeRange(0, string.length), options: NSAttributedString.EnumerationOptions(rawValue: 0)) { (value, range, stop) in
            if let attachment = value as? EmojiAttachment {
                var fixedRange = range
                fixedRange.location += offset
                let key = attachment.key ?? ""
                str.replaceCharacters(in: fixedRange, with: key)
                offset += (key as NSString).length - 1
            }
        }
        return str
    }
    
    //---------------------------------------------------
    // MARK: - Private
    //---------------------------------------------------
    
    static func resourceBundle()->Bundle? {
        if let resourceBundleURL = Bundle(for: self).url(forResource: "Emojify", withExtension: "bundle"),
            let resourceBundle = Bundle(url: resourceBundleURL) {
            return resourceBundle
        }
        return nil
    }
    
    fileprivate func loadJSON(_ fileName:String) -> AnyObject? {
        
        if let resourceBundle = GGEmoji.resourceBundle(),
            let path = resourceBundle.path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let json = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) {
            return json as AnyObject?
        }
        return nil
    }
}

//---------------------------------------------------
// MARK: - Recent History
//---------------------------------------------------

public extension GGEmoji {
    
    public func addToRecentHistory(emojiInfo: EmojiInfo) {
        if let index = recentEmojis.index(of: emojiInfo) {
            recentEmojis.remove(at: index)
        }
        recentEmojis.insert(emojiInfo, at: 0)
        
        trimRecentHistory()
        saveRecentHistory()
    }
    
    func trimRecentHistory() {
        if recentEmojis.count > recentEmojisLimit {
            _ = recentEmojis.dropLast(recentEmojis.count-recentEmojisLimit)
        }
    }
    
    func saveRecentHistory() {
        let recentArr = JSON(recentEmojis.map({ return $0.toJSON() }))
        let recentStr = recentArr.rawString()
        UserDefaults.standard.set(recentStr, forKey: "gg-emoji-recent-history")
        UserDefaults.standard.synchronize()
    }
    
    func loadRecentHistory() {
        let recentStr = UserDefaults.standard.string(forKey: "gg-emoji-recent-history")
        if let data = recentStr?.data(using: .utf8) {
            let json = JSON(data: data)
            
            recentEmojis = json.arrayValue.map({ (jsonInfo) -> EmojiInfo in
                return EmojiInfo(json: jsonInfo)
            })
        }
    }
}
