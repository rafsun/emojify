//
//  EmojiBundle.swift
//  Pods
//
//  Created by Ashik Ahmad on 2/1/17.
//
//

import Foundation
import SwiftyJSON

public class EmojiBundle {
    public internal(set) var name:String
    public internal(set) var icon:String
    public internal(set) var emojis:[EmojiInfo] = []
    
    init(name: String, iconName: String) {
        self.name = name
        icon = iconName
    }
}

public class EmojiInfo {
    public internal(set) var key:String
    public internal(set) var image:String
    public internal(set) var unicodeEmoji:String
    
    init(key: String, imageName: String, emoji: String) {
        self.key = key
        image = imageName
        unicodeEmoji = emoji
    }
}

extension EmojiBundle {
    func toJSON()->JSON {
        var json = JSON(dictionaryLiteral: ("name", name), ("icon", icon))
        let emojiArr = emojis.map { return $0.toJSON() }
        json["emojis"].arrayObject = emojiArr
        return json
    }
}

extension EmojiInfo {
    
    convenience init(json: JSON) {
        let key = json["key"].stringValue
        let image = json["image"].stringValue
        let unicodeEmoji = json["unicodeEmoji"].stringValue
        
        self.init(key: key, imageName: image, emoji: unicodeEmoji)
    }
    
    func toJSON()->JSON {
        return JSON(dictionaryLiteral: ("key", key), ("image", image), ("unicodeEmoji", unicodeEmoji))
    }
}

extension EmojiInfo: Equatable {
    public static func ==(lhs: EmojiInfo, rhs: EmojiInfo) -> Bool {
        return (lhs.key == rhs.key)
    }
}

