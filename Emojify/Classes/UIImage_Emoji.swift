//
//  UIImage_Emoji.swift
//  Pods
//
//  Created by Ashik Ahmad on 2/1/17.
//
//

import UIKit

public extension UIImage {
    public convenience init?(emoji: String) {
        self.init(named: emoji, in: GGEmoji.resourceBundle(), compatibleWith: nil)
    }
}
