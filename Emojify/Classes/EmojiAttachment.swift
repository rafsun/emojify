//
//  EmojiAttachment.swift
//  Connect
//
//  Created by Ashik Ahmad on 10/28/16.
//  Copyright © 2016 Gagagugu. All rights reserved.
//

import UIKit

class EmojiAttachment: NSTextAttachment {
    var font:UIFont?
    var verticalOffset: CGFloat { return font?.descender ?? 0 }
    var key:String?
    // To vertically center the image, pass in the font descender as the vertical offset.
    // We cannot get this info from the text container since it is sometimes nil when `attachmentBoundsForTextContainer` is called.
    
    convenience init(_ image: UIImage, key:String? = nil, font: UIFont? = nil) {
        self.init()
        self.image = image
        self.key = key
        self.font = font
    }
    
    override func attachmentBounds(for textContainer: NSTextContainer?, proposedLineFragment lineFrag: CGRect, glyphPosition position: CGPoint, characterIndex charIndex: Int) -> CGRect {
        //TODO: - height calculation needs more perfection....
        let height = (font?.lineHeight ?? lineFrag.size.height) + 1
        var scale: CGFloat = 1.0;
        let imageSize = image?.size ?? CGSize.zero
        
        if (height < imageSize.height) {
            scale = height / imageSize.height
        }
        
        return CGRect(x: 0, y: verticalOffset, width: imageSize.width * scale, height: imageSize.height * scale)
    }
}
