//
//  ViewController.swift
//  Emojify
//
//  Created by Ashik Ahmad on 01/19/2017.
//  Copyright (c) 2017 Ashik Ahmad. All rights reserved.
//

import UIKit
import Emojify

class ViewController: UIViewController, UITextViewDelegate {
    let emojiMan: GGEmoji = GGEmoji()
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        textView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //---------------------------------------------------
    // MARK: - TextView Delegate
    //---------------------------------------------------
    
    func textViewDidChange(_ textView: UITextView) {
        textView.emojify(with: emojiMan)
    }
}

