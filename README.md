# Emojify

[![CI Status](http://img.shields.io/travis/Ashik Ahmad/Emojify.svg?style=flat)](https://travis-ci.org/Ashik Ahmad/Emojify)
[![Version](https://img.shields.io/cocoapods/v/Emojify.svg?style=flat)](http://cocoapods.org/pods/Emojify)
[![License](https://img.shields.io/cocoapods/l/Emojify.svg?style=flat)](http://cocoapods.org/pods/Emojify)
[![Platform](https://img.shields.io/cocoapods/p/Emojify.svg?style=flat)](http://cocoapods.org/pods/Emojify)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Emojify is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Emojify"
```

## Author

Ashik Ahmad, ashik.ahmad@gagagugu.com

## License

Emojify is available under the MIT license. See the LICENSE file for more info.
